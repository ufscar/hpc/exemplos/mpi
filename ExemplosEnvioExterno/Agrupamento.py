import matplotlib.pyplot as plt
import hdbscan
import numpy as np
import pandas as pd
import sys

dataset = np.random.rand(1000, 2)
dataset[0:500] += 1

plt.figure(figsize=(10, 6))
plt.scatter(dataset[:,0], dataset[:,1])
# Enviar essa figura telegram/discord
plt.savefig(sys.argv[1])

clusterer = hdbscan.HDBSCAN()
clusterer.fit(dataset)

plt.figure(figsize=(10, 6))
plt.scatter(dataset[:,0], dataset[:,1], c=clusterer.labels_)
# Enviar essa figura telegram/discord
plt.savefig(sys.argv[2])

pd.DataFrame(clusterer.labels_).to_csv(sys.argv[3])


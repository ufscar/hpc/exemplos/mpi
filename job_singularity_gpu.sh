#!/bin/bash
#SBATCH -J comparador             # Job name
#SBATCH -o %j.out                 # Name of stdout output
#SBATCH -w c11                    # Node requested
#SBATCH --partition=fast
#SBATCH --ntasks=1                # Total number of processors
#SBATCH --time=00:15:00           # Run time (hh:mm:ss) - 15 min
#SBATCH --gres=gpu:turing:1       # GPU
#SBATCH --mem=100G
#SBATCH --mail-user=afsmaira@ufscar.br
#SBATCH --mail-type=ALL

service=cloud                               # Nome do perfil remoto no rclone
remote_in=hpc/input                         # Pasta no serviço remoto para input
remote_out=hpc/output                       # Pasta no serviço remoto para output
remote_sing=hpc/containers/ufscar/template  # Pasta no serviço remoto para os containers
container_in=/opt/input                     # Pasta no cluster para input
container_out=/opt/output                   # Pasta no cluster para output
local_job="/scratch/job.${SLURM_JOB_ID}"    # Pasta temporária local
local_in="${local_job}/input/"              # Pasta local para arquivos de entrada
local_out="${local_job}/output/"            # Pasta local para arquivos de saída

# Execução do singularity
singRun="singularity run --bind=/scratch:/scratch --bind=/var/spool/slurm:/var/spool/slurm"
# Executa apenas uma vez
justOne="srun --ntasks=1 --ntasks-per-node=1 --cpus-per-task=1"
# Executa o singularity na GPU
runSing="${justOne} --gres=gpu:turing:1 ${singRun} --nv gpu.sif"

function clean_job() {
  echo "Limpando ambiente..."
  ${justOne} rm -rf "${local_job}"
}
trap clean_job EXIT HUP INT TERM ERR

set -eE

umask 077

# Define o arquivo a ser copiado como o mais recente na pasta especificada
sing=$(rclone lsf --max-depth 0 "${service}:${remote_sing}/" --files-only --format "tp" | grep simg | grep example | sort | tail -1)
sing=${sing:20}

echo "Criando pastas temporárias..."
${justOne} mkdir -p "${local_in}"
${justOne} mkdir -p "${local_out}"

echo "Copiando container..."
${justOne} rclone copyto "${service}:${remote_sing}/${sing}" "${local_job}/Singularity.simg" --multi-thread-streams "${SLURM_CPUS_ON_NODE}"

echo "Copiando input..."
${justOne} rclone copy "${service}:${remote_in}/" "${local_in}/" --transfers "${SLURM_CPUS_ON_NODE}"

cd ${local_job}

echo "Executando..."
${runSing}

echo "Enviando output..."
${justOne} rclone move "${local_out}" "${service}:${remote_out}/" --transfers "${SLURM_CPUS_ON_NODE}"
${justOne} rclone move "${SLURM_JOB_ID}.out" "${service}:${remote_out}/" --transfers "${SLURM_CPUS_ON_NODE}"
